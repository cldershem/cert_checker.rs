extern crate openssl;
use openssl::ssl::{SslMethod, SslConnectorBuilder};
use std::net::{TcpStream};

extern crate  reqwest;

use std::io::{Read};

mod errors;
use errors::URLError;

extern crate chrono;


fn check_url(url: &str) -> Result<String, URLError> {
    let mut with_protocol = String::new();
    with_protocol.push_str("https://");
    with_protocol.push_str(url);
    let url_with_protocol: &str = with_protocol.as_str();

    let mut response = reqwest::get(url_with_protocol)?;

    let mut buf = String::new();
    response.read_to_string(&mut buf)?;
    println!("{} is good.", url);
    Ok(buf)
}

fn check_correct_page(response: &mut String, check_str: &str) -> bool {
    let status = response.contains(check_str);
    match status {
        true => println!("contains {}.", check_str),
        false => println!("doesn't contain {}.", check_str),
    }
    status
}

fn get_cert_expiration(url: &str) -> Result<bool, URLError> {
    let connector = SslConnectorBuilder::new(SslMethod::tls()).unwrap().build();

    let context = TcpStream::connect((url, 443))?;
    let stream = connector.connect(url, context).unwrap();
    let cert = stream.ssl().peer_certificate().unwrap();
    let not_after = cert.not_after();

    // parse notAfter into datetime
    let time_str = not_after.to_string();
    let (time_no_zone, _) = time_str.split_at(20);
    let time_with_zone = time_no_zone.to_string() + " +0000";
    let parsed_time = chrono::DateTime::parse_from_str(&time_with_zone, "%b %d %H:%M:%S %Y %z").unwrap();

    let now = chrono::UTC::now();
    let two_weeks = 1209600;

    // if (parsed_time - now).num_seconds() < two_weeks {
    //     Err("aaa")
    // }
    Ok(true)
}

fn main() {
    let url = "www.pinkhatbeard.com";

    let check_str = "Cameron";
    let bad_cert = "expired.badssl.com";
    let bad_url = "thisisaurlthat.doesntwork.com";

    for url in vec![url, bad_cert, bad_url] {
        match check_url(url) {
            Ok(mut response) => {
                match check_correct_page(&mut response, check_str) {
                    true => {
                        match get_cert_expiration(url) {
                            Ok(_) => println!("Cert OK"),
                            Err(err) => println!("Cert Err: {}", err),
                        }
                    }
                    false => println!("{} does not contain {}.", url, check_str),
                }
            }
            Err(err) => println!("{} is bad. {}", url, err),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::check_url;
    use super::check_correct_page;

    #[test]
    fn test_good_url() {
        let url = "www.google.com";
        let result = check_url(url);

        assert!(result.is_err(), false);
        // assert!(result.is_ok(), true);
    }

    #[test]
    fn test_bad_url() {
        let url = "www.somebadurlthatdoesntwork.com";
        let result = check_url(url);
        assert!(result.is_err(), true);
    }

    #[test]
    fn test_correct_page() {
        let url = "www.pinkhatbeard.com";
        let check_str = "Cameron";
        let mut response = check_url(url).unwrap();

        assert!(check_correct_page(&mut response, check_str), true);
    }

    #[test]
    fn test_incorrect_page() {
        let url = "www.pinkhatbeard.com";
        let check_str = "taco taco taco why would it have tacos?";
        let mut response = check_url(url).unwrap();

        assert!(check_correct_page(&mut response, check_str), false);
    }
}
