use std::error::Error;
use std::fmt;
use reqwest;
use std::io;
use openssl;


#[derive(Debug)]
pub enum URLError {
    Reqwest(reqwest::Error),
    IO(io::Error),
    SSL(openssl::error::Error),
}

impl From<reqwest::Error> for URLError {
    fn from(err: reqwest::Error) -> URLError {
        URLError::Reqwest(err)
    }
}

impl From<io::Error> for URLError {
    fn from(err: io::Error) -> URLError {
        URLError::IO(err)
    }
}

impl From<openssl::error::Error> for URLError {
    fn from(err: openssl::error::Error) -> URLError {
        URLError::SSL(err)
    }
}

impl fmt::Display for URLError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            // URLError::Hyper(ref err) => write!(f, "Hyper Error: {}", err),
            URLError::Reqwest(ref err) => err.fmt(f),
            URLError::IO(ref err) => err.fmt(f),
            URLError::SSL(ref err) => err.fmt(f),
        }
    }
}

impl Error for URLError {
    fn description(&self) -> &str {
        match *self {
            URLError::Reqwest(ref err) => err.description(),
            URLError::IO(ref err) => err.description(),
            URLError::SSL(ref err) => err.description(),
        }
    }

    fn cause(&self) -> Option<&Error> {
        match *self {
            URLError::Reqwest(ref err) => Some(err),
            URLError::IO(ref err) => Some(err),
            URLError::SSL(ref err) => Some(err),
        }
    }
}
